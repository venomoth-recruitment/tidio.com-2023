USE tidio;

SET @hr_bonus_id = UUID();
SET @customer_service_bonus_id = UUID();

SET @hr_department_id = UUID();
SET @customer_service_department_id = UUID();

SET @payroll_report_id = 'b0ecb638-7e99-4d68-a0ea-5f94b7cd3043';

INSERT INTO `bonuses` VALUES
(UUID_TO_BIN(@hr_bonus_id), 'HR', 'fixed', 100),
(UUID_TO_BIN(@customer_service_bonus_id), 'Customer Service', 'percentage', 10);

INSERT INTO `departments` VALUES
(UUID_TO_BIN(@hr_department_id), UUID_TO_BIN(@hr_bonus_id), 'HR'),
(UUID_TO_BIN(@customer_service_department_id), UUID_TO_BIN(@customer_service_bonus_id), 'Customer Service');

INSERT INTO `employees` VALUES
(UUID_TO_BIN(UUID()), UUID_TO_BIN(@hr_department_id), 'Adam', 'Kowalski', 1000, '2008-01-01'),
(UUID_TO_BIN(UUID()), UUID_TO_BIN(@customer_service_department_id), 'Ania', 'Nowak', 1100, '2018-01-01');

INSERT INTO `payroll_reports` VALUES
(UUID_TO_BIN(@payroll_report_id), '2023-01-31');

INSERT INTO `payroll_report_details` VALUES
(UUID_TO_BIN(UUID()), UUID_TO_BIN(@payroll_report_id), 'Adam', 'Kowalski', 'HR', 1000, 'fixed', 1000, 2000),
(UUID_TO_BIN(UUID()), UUID_TO_BIN(@payroll_report_id), 'Ania', 'Nowak', 'Customer Service', 1100, 'percentage', 110, 1210);
