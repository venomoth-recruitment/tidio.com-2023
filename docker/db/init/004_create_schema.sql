USE tidio;

DROP TABLE IF EXISTS `payroll_report_details`;
DROP TABLE IF EXISTS `payroll_reports`;
DROP TABLE IF EXISTS `employees`;
DROP TABLE IF EXISTS `departments`;
DROP TABLE IF EXISTS `bonuses`;

CREATE TABLE `bonuses` (
    `id` binary(16) NOT NULL,
    `name` varchar(30) NOT NULL,
    `type` enum('fixed', 'percentage') NOT NULL,
    `value` int UNSIGNED NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `departments` (
    `id` binary(16) NOT NULL,
    `bonus_id` binary(16) NOT NULL,
    `name` varchar(30) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT FK_departments_bonuses
    FOREIGN KEY (`bonus_id`) REFERENCES `bonuses`(`id`)
) ENGINE=InnoDB;

CREATE TABLE `employees` (
    `id` binary(16) NOT NULL,
    `department_id` binary(16) NOT NULL,
    `name` varchar(30) NOT NULL,
    `surname` varchar(30) NOT NULL,
    `salary` int UNSIGNED NOT NULL,
    `employed_at` date NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT FK_employees_departments
    FOREIGN KEY (`department_id`) REFERENCES `departments`(`id`)
) ENGINE=InnoDB;

CREATE TABLE `payroll_reports` (
    `id` binary(16) NOT NULL,
    `date` date NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `payroll_report_details` (
    `id` binary(16) NOT NULL,
    `payroll_report_id` binary(16) NOT NULL,
    `employee_name` varchar(30) NOT NULL,
    `employee_surname` varchar(30) NOT NULL,
    `department` varchar(30) NOT NULL,
    `base_salary` int UNSIGNED NOT NULL,
    `bonus_type` varchar(30) NOT NULL,
    `bonus` int UNSIGNED NOT NULL,
    `total_salary` int UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT FK_payroll_report_details_payroll_reports
    FOREIGN KEY (`payroll_report_id`) REFERENCES `payroll_reports`(`id`),
    INDEX `payroll_report_details_employee_name_index` (`employee_name`),
    INDEX `payroll_report_details_employee_surname_index` (`employee_surname`),
    INDEX `payroll_report_details_department_index` (`department`)
) ENGINE=InnoDB;
