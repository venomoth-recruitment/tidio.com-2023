<?php 

declare(strict_types=1);

namespace App\Tests\UI\API\Controller;

use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PayrollReportControllerTest extends WebTestCase
{
    public function testGetAll(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/payroll_reports/b0ecb638-7e99-4d68-a0ea-5f94b7cd3043');

        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();

        $this->assertEquals(
            '[{"name":"Adam","surname":"Kowalski","department":"HR","base_salary":1000,"bonus_type":"fixed","bonus":1000,"total_salary":2000},{"name":"Ania","surname":"Nowak","department":"Customer Service","base_salary":1100,"bonus_type":"percentage","bonus":110,"total_salary":1210}]',
            $response->getContent()
        );
    }

    /**
     * @dataProvider filtersDataProvider
     */
    public function testFilters(string $query, string $expectedResponse): void
    {
        $expectedContent = match ($expectedResponse) {
            'ania' => '[{"name":"Ania","surname":"Nowak","department":"Customer Service","base_salary":1100,"bonus_type":"percentage","bonus":110,"total_salary":1210}]',
            'adam' => '[{"name":"Adam","surname":"Kowalski","department":"HR","base_salary":1000,"bonus_type":"fixed","bonus":1000,"total_salary":2000}]',
            'not_found' => '',
            default => throw new InvalidArgumentException('Unknown option')
        };

        $client = static::createClient();

        $crawler = $client->request('GET', '/payroll_reports/b0ecb638-7e99-4d68-a0ea-5f94b7cd3043?' . $query);

        $response = $client->getResponse();

        $expectedResponse === 'not_found' ? $this->assertResponseStatusCodeSame(404) : $this->assertResponseIsSuccessful();

        $this->assertEquals($expectedContent, $response->getContent());
    }

    private function filtersDataProvider(): array
    {
        return [
            ['name=ania', 'ania'],
            ['surname=nowak', 'ania'],
            ['department=customer service', 'ania'],
            ['name=adam', 'adam'],
            ['surname=kowalski', 'adam'],
            ['department=hr', 'adam'],
            ['name=ada', 'not_found']
        ];
    }

    /**
     * @dataProvider badQueryDataProvider
     */
    public function testBadQuery(string $query): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/payroll_reports/b0ecb638-7e99-4d68-a0ea-5f94b7cd3043?' . $query);

        $response = $client->getResponse();

        $this->assertResponseStatusCodeSame(400);
    }

    private function badQueryDataProvider(): array
    {
        return [
            ['name='],
            ['surname='],
            ['department='],
            ['name=namemorethan30characters!!!!!!!'],
            ['surname=surnamemorethan30characters!!!!'],
            ['department=departmentmorethan30characters!'],
            ['sortBy='],
            ['orderBy='],
            ['sortBy=unknown&orderBy=asc'],
            ['sortBy=unknown&orderBy=desc'],
            ['sortBy=name&orderBy=aasc'],
            ['unknown=1']
        ];
    }

    /**
     * @dataProvider sortDataProvider
     */
    public function testSort(string $query, $expectedResponse): void
    {
        $expectedContent = match ($expectedResponse) {
            'ania_first' => '[{"name":"Ania","surname":"Nowak","department":"Customer Service","base_salary":1100,"bonus_type":"percentage","bonus":110,"total_salary":1210},{"name":"Adam","surname":"Kowalski","department":"HR","base_salary":1000,"bonus_type":"fixed","bonus":1000,"total_salary":2000}]',
            'adam_first' => '[{"name":"Adam","surname":"Kowalski","department":"HR","base_salary":1000,"bonus_type":"fixed","bonus":1000,"total_salary":2000},{"name":"Ania","surname":"Nowak","department":"Customer Service","base_salary":1100,"bonus_type":"percentage","bonus":110,"total_salary":1210}]',
            default => throw new InvalidArgumentException('Unknown option')
        };

        $client = static::createClient();

        $crawler = $client->request('GET', '/payroll_reports/b0ecb638-7e99-4d68-a0ea-5f94b7cd3043?' . $query);

        $response = $client->getResponse();

        $expectedResponse === 'not_found' ? $this->assertResponseStatusCodeSame(404) : $this->assertResponseIsSuccessful();

        $this->assertEquals($expectedContent, $response->getContent());
    }

    private function sortDataProvider(): array
    {
        return [
            ['sort_by=name&order_by=asc', 'adam_first'],
            ['sort_by=name&order_by=desc', 'ania_first'],
            ['sort_by=surname&order_by=asc', 'adam_first'],
            ['sort_by=surname&order_by=desc', 'ania_first'],
            ['sort_by=department&order_by=asc', 'ania_first'],
            ['sort_by=department&order_by=desc', 'adam_first'],
            ['sort_by=base_salary&order_by=asc', 'adam_first'],
            ['sort_by=base_salary&order_by=desc', 'ania_first'],
            ['sort_by=bonus_type&order_by=asc', 'adam_first'],
            ['sort_by=bonus_type&order_by=desc', 'ania_first'],
            ['sort_by=bonus&order_by=asc', 'ania_first'],
            ['sort_by=bonus&order_by=desc', 'adam_first'],
            ['sort_by=total_salary&order_by=asc', 'ania_first'],
            ['sort_by=total_salary&order_by=desc', 'adam_first'],
        ];
    }
}
