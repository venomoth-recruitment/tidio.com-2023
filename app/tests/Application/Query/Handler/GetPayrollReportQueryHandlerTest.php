<?php 

declare(strict_types=1);

namespace App\Tests\Application\Query\Handler;

use App\Application\Query\GetPayrollReportQuery;
use App\Application\Query\Handler\GetPayrollReportQueryHandler;
use App\Application\Query\View\GetPayrollReportView;
use App\Domain\PayrollReport\Collection\PayrollReportDetails;
use App\Domain\PayrollReport\PayrollReportAggregate;
use App\Domain\PayrollReport\Repository\PayrollReportRepositoryInterface;
use App\Domain\Shared\Criteria\Criteria;
use App\Domain\Shared\Criteria\FilterOperator;
use App\Domain\Shared\Criteria\Filters;
use App\Domain\Shared\Criteria\Order;
use App\Domain\Shared\Criteria\OrderBy;
use App\Domain\Shared\Criteria\SortBy;
use App\Domain\Shared\ValueObject\IdValueObject;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class GetPayrollReportQueryHandlerTest extends TestCase
{
    public function testHandle(): void
    {
        $reportId = 'b0ecb638-7e99-4d68-a0ea-5f94b7cd3043';
        $detailId = 'a3ecb638-7e00-4d68-a0ea-5f94b7cd3027';
        $employeeName = 'name';
        $employeeSurname = 'surname';
        $department = 'department';
        $baseSalary = 1000;
        $bonusType = 'fixed';
        $bonus = 10;
        $totalSalary = 1010;

        $filters = [
            [
                'field' => 'employee_name',
                'operator' => FilterOperator::EQUAL,
                'value' => $employeeName
            ],
            [
                'field' => 'employee_surname',
                'operator' => FilterOperator::EQUAL,
                'value' => $employeeSurname
            ],
            [
                'field' => 'department',
                'operator' => FilterOperator::EQUAL,
                'value' => $department
            ]
        ];

        $order = new Order(new SortBy('employee_name'), OrderBy::asc);

        $query = new GetPayrollReportQuery($reportId, $employeeName, $employeeSurname, $department, 'name', 'asc');

        $payrollReportAggregate = new PayrollReportAggregate(
            new IdValueObject($reportId),
            new DateTimeImmutable(),
            PayrollReportDetails::fromValues([
                [
                    'id' => $detailId,
                    'employee_name' => $employeeName,
                    'employee_surname' => $employeeSurname,
                    'department' => $department,
                    'base_salary' => $baseSalary,
                    'bonus_type' => $bonusType,
                    'bonus' => $bonus,
                    'total_salary' => $totalSalary
                ]
            ])
        );

        $expectedIdValueObject = new IdValueObject($reportId);
        $expectedCriteria = new Criteria(
            Filters::fromValues($filters),
            $order
        );

        $payrollReportRepository = $this->createMock(PayrollReportRepositoryInterface::class);
        $payrollReportRepository->expects(self::once())
            ->method('findBy')
            ->with($this->equalTo($expectedIdValueObject), $this->equalTo($expectedCriteria))
            ->willReturn($payrollReportAggregate);

        $expectedView = GetPayrollReportView::fromPayrollReportAggregate($payrollReportAggregate);

        $sut = new GetPayrollReportQueryHandler($payrollReportRepository);
        $actualView = $sut->handle($query);

        $this->assertEquals($expectedView, $actualView);
    }
}
