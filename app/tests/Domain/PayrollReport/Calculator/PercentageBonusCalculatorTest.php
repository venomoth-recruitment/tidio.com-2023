<?php 

declare(strict_types=1);

namespace App\Tests\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\Calculator\PercentageBonusCalculator;
use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use App\Domain\PayrollReport\ValueObject\SalaryValueObject;
use PHPUnit\Framework\TestCase;

class PercentageBonusCalculatorTest extends TestCase
{
    /**
     * @dataProvider calculateDataProvider
     */
    public function testCalculate(int $salary, int $bonus, int $expectedBonus): void
    {
        $sut = new PercentageBonusCalculator();

        $actualBonus = $sut->calculate(new SalaryValueObject($salary), new BonusValueObject($bonus));

        $this->assertSame($expectedBonus, $actualBonus->value());
    }

    private function calculateDataProvider(): array
    {
        return [
            [30, 1, 0],
            [200, 5, 10],
            [500, 2, 10],
            [2333, 9, 209],
            [4400, 10, 440],
            [44, 1000, 440]
        ];
    }
}
