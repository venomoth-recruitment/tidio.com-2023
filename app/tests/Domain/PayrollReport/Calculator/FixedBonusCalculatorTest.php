<?php 

declare(strict_types=1);

namespace App\Tests\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\Calculator\FixedBonusCalculator;
use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class FixedBonusCalculatorTest extends TestCase
{
    /**
     * @dataProvider calculateDataProvider
     */
    public function testCalculate(int $bonus, int $yearsOfEmployment, int $expectedBonus): void
    {
        $sut = new FixedBonusCalculator();

        $actualBonus = $sut->calculate(new BonusValueObject($bonus), new DateTimeImmutable("now - {$yearsOfEmployment} years"));

        $this->assertSame($expectedBonus, $actualBonus->value());
    }

    private function calculateDataProvider(): array
    {
        return [
            [3, 1, 3],
            [2, 5, 10],
            [5, 2, 10],
            [2333, 9, 20997],
            [44, 10, 440],
            [44, 11, 440],
            [44, 1000, 440]
        ];
    }
}
