<?php

declare(strict_types=1);

namespace App\UI\API\Controller;

use App\Application\Query\GetPayrollReportQuery;
use App\Application\Query\Handler\GetPayrollReportQueryHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;


class PayrollReportController extends AbstractController
{
    public function __construct(
        private readonly GetPayrollReportQueryHandlerInterface $getPayrollReportQueryHandler
    ) {
    }

    #[Route('/payroll_reports/{id}', methods: ['GET'])]
    public function show(string $id, Request $request): Response
    {
        $request->query->add(['id' => $id]);

        $violations = $this->validateRequest($request);

        if ($violations->count() > 0) {
            return new JsonResponse([
                $violations->get(0)->getPropertyPath() => $violations->get(0)->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        $requestData = $request->query->all();

        $query = new GetPayrollReportQuery(
            $requestData['id'],
            $requestData['name'] ?? null,
            $requestData['surname'] ?? null,
            $requestData['department'] ?? null,
            $requestData['sort_by'] ?? null,
            $requestData['order_by'] ?? null,
        );
        
        $getPayrollReportView = $this->getPayrollReportQueryHandler->handle($query);

        if (empty($getPayrollReportView->data)) {
            return new Response(status: Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($getPayrollReportView->data);
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {
        $constraint = new Constraints\Collection([
            'id' => new Constraints\Uuid(),
            'name' => new Constraints\Optional([
                new Constraints\NotBlank(),
                new Constraints\Length(['max' => 30])
            ]),
            'surname' => new Constraints\Optional([
                new Constraints\NotBlank(),
                new Constraints\Length(['max' => 30])
            ]),
            'department' => new Constraints\Optional([
                new Constraints\NotBlank(),
                new Constraints\Length(['max' => 30])
            ]),
            'sort_by' => new Constraints\Optional([
                new Constraints\NotBlank(),
                new Constraints\Choice(['name', 'surname', 'department', 'base_salary', 'bonus', 'bonus_type', 'total_salary'])
            ]),
            'order_by' => new Constraints\Optional([
                new Constraints\NotBlank(),
                new Constraints\Choice(['asc', 'desc'])
            ]),
        ]);

        $validator = Validation::createValidator();

        return $validator->validate($request->query->all(), $constraint);
    }
}
