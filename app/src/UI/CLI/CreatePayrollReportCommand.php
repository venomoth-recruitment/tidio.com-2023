<?php

declare(strict_types=1);

namespace App\UI\CLI;

use App\Application\Command\CreatePayrollReportCommand as CreatePayrollReportApplicationCommand;
use App\Application\Command\Handler\CreatePayrollReportCommandHandlerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:create-payroll-report',
    description: 'Creates payroll report for current date.',
    hidden: false
)]
class CreatePayrollReportCommand extends Command
{
    public function __construct(
        private readonly CreatePayrollReportCommandHandlerInterface $createPayrollReportCommandHandler
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $command = new CreatePayrollReportApplicationCommand();
        
        $this->createPayrollReportCommandHandler->handle($command);

        $output->writeln('<info>Report created successfuly!</info>');

        return Command::SUCCESS;
    }
}
