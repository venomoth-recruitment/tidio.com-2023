<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository;

use App\Domain\PayrollReport\Collection\PayrollReportDetails;
use App\Domain\PayrollReport\PayrollReportAggregate;
use App\Domain\PayrollReport\Repository\PayrollReportRepositoryInterface;
use App\Domain\Shared\Criteria\Criteria;
use App\Domain\Shared\ValueObject\IdValueObject;
use App\Infrastructure\Domain\PayrollReport\Repository\Mapper\PayrollReportDetailsMapperInterface;
use DateTimeImmutable;
use Doctrine\DBAL\Statement;
use Doctrine\ORM\EntityManagerInterface;

class MySqlPayrollReportRepository implements PayrollReportRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly PayrollReportDetailsMapperInterface $mapper
    ) {
    }

    public function store(PayrollReportAggregate $payrollReport): void
    {
        $sql = "INSERT INTO payroll_reports VALUES (UUID_TO_BIN(:id), :date);";

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->bindValue('id', $payrollReport->getId()->value());
        $stmt->bindValue('date', $payrollReport->getDate()->format('Y-m-d'));
        $stmt->executeQuery();

        if ($payrollReport->getDetails()->count() > 0) {
            $sql = sprintf("INSERT INTO payroll_report_details VALUES %s;",
                $this->mapper->toSql($payrollReport->getId(), $payrollReport->getDetails())
            );

            $stmt = $this->entityManager->getConnection()->prepare($sql);
            $stmt->executeQuery();
        }
    }

    public function findBy(IdValueObject $id, Criteria $criteria): ?PayrollReportAggregate
    {
        $sql = "SELECT date FROM payroll_reports WHERE id = UUID_TO_BIN(:id);";

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id->value());
        $result = $stmt->executeQuery()->fetchAllAssociative();

        if (empty($result)) {
            return null;
        }

        $payrollReportDate = DateTimeImmutable::createFromFormat('Y-m-d', $result[0]['date']);

        $sql = "
            SELECT
                BIN_TO_UUID(id) id,
                employee_name,
                employee_surname,
                department,
                base_salary,
                bonus_type,
                bonus,
                total_salary
            FROM
                payroll_report_details
            WHERE
                payroll_report_id = UUID_TO_BIN(:id)
            ";
        
        $bindValues = $this->attachCriteriaToSql($sql, $criteria);
        $bindValues['id'] = $id->value();

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $this->bindValues($stmt, $bindValues);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        return new PayrollReportAggregate(
            $id,
            $payrollReportDate,
            PayrollReportDetails::fromValues($result)
        );
    }

    private function attachCriteriaToSql(string &$sql, Criteria $criteria): array
    {
        $values = [];

        if ($criteria->hasFilters()) {
            foreach ($criteria->filters() as $filter) {
                $sql .= sprintf(" AND %s %s :%s",
                    $filter->field()->value,
                    $filter->operator()->value,
                    $filter->field()->value
                );

                $values[$filter->field()->value] = $filter->value()->value;
            }
        }

        if ($criteria->hasOrder()) {
            $sql .= sprintf(" ORDER BY %s %s",
                $criteria->order()->sortBy->value,
                $criteria->order()->orderBy->name
            );
        }

        return $values;
    }

    private function bindValues(Statement $stmt, array $values): void
    {
        foreach ($values as $name => $value) {
            $stmt->bindValue($name, $value);
        }
    }
}
