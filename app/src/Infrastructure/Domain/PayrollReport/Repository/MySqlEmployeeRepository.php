<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository;

use App\Domain\PayrollReport\Collection\Employees;
use App\Domain\PayrollReport\Repository\EmployeeRepositoryInterface;
use App\Infrastructure\Domain\PayrollReport\Repository\Mapper\EmployeesMapperInterface;
use Doctrine\ORM\EntityManagerInterface;

class MySqlEmployeeRepository implements EmployeeRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly EmployeesMapperInterface $mapper
    ) {
    }

    public function findAll(): Employees
    {
        $sql = "
            SELECT
                BIN_TO_UUID(e.id) id,
                e.name,
                e.surname,
                e.salary,
                e.employed_at,
                BIN_TO_UUID(d.id) department_id,
                d.name department_name,
                BIN_TO_UUID(b.id) bonus_id,
                b.name bonus_name,
                b.type bonus_type,
                b.value bonus_value
            FROM
                employees e
            JOIN
                departments d
            ON
                e.department_id = d.id
            JOIN
                bonuses b
            ON
                d.bonus_id = b.id;
        ";

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        return $this->mapper->fromSql($result);
    }
}
