<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository\Mapper;

use App\Domain\PayrollReport\Collection\PayrollReportDetails;
use App\Domain\Shared\ValueObject\IdValueObject;

interface PayrollReportDetailsMapperInterface
{
    public function toSql(IdValueObject $id, PayrollReportDetails $payrollReportDetails): string;
}
