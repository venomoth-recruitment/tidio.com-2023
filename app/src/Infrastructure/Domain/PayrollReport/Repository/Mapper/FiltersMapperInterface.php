<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository\Mapper;

use App\Domain\PayrollReport\Collection\Employees;

interface FiltersMapperInterface
{
    public function toSql(array $result): Employees;
}
