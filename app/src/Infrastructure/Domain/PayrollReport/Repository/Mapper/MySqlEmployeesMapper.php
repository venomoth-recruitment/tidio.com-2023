<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository\Mapper;

use App\Domain\PayrollReport\Collection\Employees;

class MySqlEmployeesMapper implements EmployeesMapperInterface
{
    public function fromSql(array $result): Employees
    {
        return Employees::fromValues(array_map(fn (array $row): array => 
            [
                'id' => $row['id'],
                'name' => $row['name'],
                'surname' => $row['surname'],
                'salary' => $row['salary'],
                'employed_at' => $row['employed_at'],
                'department' => [
                    'id' => $row['department_id'],
                    'name' => $row['department_name'],
                    'bonus' => [
                        'id' => $row['bonus_id'],
                        'name' => $row['bonus_name'],
                        'type' => $row['bonus_type'],
                        'value' => $row['bonus_value']
                    ]
                ]
            ]
        , $result));
    }
}
