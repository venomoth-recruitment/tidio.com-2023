<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository\Mapper;

use App\Domain\PayrollReport\Collection\Employees;

interface EmployeesMapperInterface
{
    public function fromSql(array $result): Employees;
}
