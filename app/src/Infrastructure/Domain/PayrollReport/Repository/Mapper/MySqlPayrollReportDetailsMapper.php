<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\PayrollReport\Repository\Mapper;

use App\Domain\PayrollReport\Collection\PayrollReportDetails;
use App\Domain\Shared\ValueObject\IdValueObject;

class MySqlPayrollReportDetailsMapper implements PayrollReportDetailsMapperInterface
{
    public function toSql(IdValueObject $payrollReportId, PayrollReportDetails $payrollReportDetails): string
    {
        $sql = '';

        foreach ($payrollReportDetails as $payrollReportDetail) {
            $sql .= sprintf("(UUID_TO_BIN('%s'), UUID_TO_BIN('%s'), '%s', '%s', '%s', %u, '%s', %u, %u),",
                $payrollReportDetail->getId()->value(),
                $payrollReportId->value(),
                $payrollReportDetail->getEmployeeName()->value(),
                $payrollReportDetail->getEmployeeSurname()->value(),
                $payrollReportDetail->getDepartmentName()->value(),
                $payrollReportDetail->getSalary()->value(),
                $payrollReportDetail->getBonusType()->name,
                $payrollReportDetail->getBonus()->value(),
                $payrollReportDetail->getTotalSalary()->value(),
            );
        }

        return substr($sql, 0, -1);
    }
}
