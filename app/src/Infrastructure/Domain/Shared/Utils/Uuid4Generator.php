<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Shared\Utils;

use App\Domain\Shared\Utils\UuidGeneratorInterface;
use Ramsey\Uuid\Uuid;

class Uuid4Generator implements UuidGeneratorInterface
{
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}
