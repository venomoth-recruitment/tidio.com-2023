<?php

declare(strict_types=1);

namespace App\Domain\Shared\Utils;

interface UuidGeneratorInterface
{
    public function generate(): string;
}
