<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

final readonly class Order
{
    public function __construct(public SortBy $sortBy, public OrderBy $orderBy)
    {
    }
}
