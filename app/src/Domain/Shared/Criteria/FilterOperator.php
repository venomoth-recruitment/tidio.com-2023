<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

enum FilterOperator: string
{
    case EQUAL = '=';
    case NOT_EQUAL = '!=';
}
