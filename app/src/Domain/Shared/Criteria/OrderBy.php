<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

enum OrderBy
{
    case asc;
    case desc;
}
