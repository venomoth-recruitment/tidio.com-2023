<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

final readonly class SortBy
{
    public function __construct(public string $value)
    {
    }
}
