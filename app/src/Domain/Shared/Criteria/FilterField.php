<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

final readonly class FilterField
{
    public function __construct(public string $value)
    {
    }
}
