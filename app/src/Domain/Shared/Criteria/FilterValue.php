<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

final readonly class FilterValue
{
    public function __construct(public string $value)
    {
    }
}
