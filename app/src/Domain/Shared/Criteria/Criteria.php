<?php

declare(strict_types=1);

namespace App\Domain\Shared\Criteria;

final class Criteria
{
    public function __construct(
        private readonly Filters $filters,
        private readonly ?Order $order
    ) {
    }

    public function hasFilters(): bool
    {
        return $this->filters->count() > 0;
    }

    public function hasOrder(): bool
    {
        return $this->order !== null;
    }

    public function filters(): Filters
    {
        return $this->filters;
    }

    public function order(): ?Order
    {
        return $this->order;
    }
}
