<?php

declare(strict_types=1);

namespace App\Domain\Shared\ValueObject;

use InvalidArgumentException;

abstract class StringValueObject
{
    public function __construct(protected readonly string $value)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException(sprintf("'%s' is too long.", $value));
        }
    }

    public function value(): string
    {
        return $this->value;
    }

    private function isValid(string $value): bool
    {
        return strlen($value) <= $this->maxLength();
    }

    abstract protected function maxLength(): int;
}
