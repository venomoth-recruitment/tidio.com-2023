<?php

declare(strict_types=1);

namespace App\Domain\Shared\ValueObject;

use InvalidArgumentException;
use Ramsey\Uuid\Uuid;

class IdValueObject
{
    public function __construct(protected readonly string $value)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException(sprintf("'%s' is not valid UUID.", $value));
        }
    }

    public function value(): string
    {
        return $this->value;
    }

    private function isValid(string $value): bool
    {
        return Uuid::isValid($value);
    }
}
