<?php

declare(strict_types=1);

namespace App\Domain\Shared\ValueObject;

use InvalidArgumentException;

abstract class IntValueObject
{
    public function __construct(protected readonly int $value)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException(sprintf("'%s' is less than %s or larger than %s.",
                $value,
                $this->minValue(),
                $this->maxValue()
            ));
        }
    }

    public function value(): int
    {
        return $this->value;
    }

    private function isValid(int $value): bool
    {
        return $value >= $this->minValue() && $value <= $this->maxValue();
    }

    abstract protected function minValue(): int;

    abstract protected function maxValue(): int;
}
