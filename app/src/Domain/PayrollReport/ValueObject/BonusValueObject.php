<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\ValueObject;

use App\Domain\Shared\ValueObject\IntValueObject;

class BonusValueObject extends IntValueObject
{
    protected function minValue(): int
    {
        return 0;
    }

    protected function maxValue(): int
    {
        return PHP_INT_MAX;
    }
}
