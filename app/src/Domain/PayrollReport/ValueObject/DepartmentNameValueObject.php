<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\ValueObject;

use App\Domain\Shared\ValueObject\StringValueObject;

class DepartmentNameValueObject extends StringValueObject
{
    protected function maxLength(): int
    {
        return 30;
    }
}
