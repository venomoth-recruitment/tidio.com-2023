<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\ValueObject;

enum BonusTypeValueObject
{
    case fixed;
    case percentage;
}
