<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Collection;

use App\Domain\PayrollReport\Entity\Employee;
use App\Domain\Shared\Collection;

final class Employees extends Collection
{
    public static function fromValues(array $values): self
    {
        return new self(array_map(self::employeeBuilder(), $values));
    }

    private static function employeeBuilder(): callable
    {
        return fn (array $values) => Employee::fromValues($values);
    }

    public function add(Employee $employee): self
    {
        return new self(array_merge($this->items(), [$employee]));
    }

    public function employees(): array
    {
        return $this->items();
    }

    protected function type(): string
    {
        return Employee::class;
    }
}
