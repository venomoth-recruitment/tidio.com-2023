<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Collection;

use App\Domain\PayrollReport\Entity\PayrollReportDetail;
use App\Domain\Shared\Collection;

final class PayrollReportDetails extends Collection
{
    public static function fromValues(array $values): self
    {
        return new self(array_map(self::payrollReportDetailBuilder(), $values));
    }

    private static function payrollReportDetailBuilder(): callable
    {
        return fn (array $values) => PayrollReportDetail::fromValues($values);
    }

    public function add(PayrollReportDetail $payrollReportDetail): self
    {
        return new self(array_merge($this->items(), [$payrollReportDetail]));
    }

    public function payrollReportDetails(): array
    {
        return $this->items();
    }

    protected function type(): string
    {
        return PayrollReportDetail::class;
    }
}
