<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\Entity\Employee;
use App\Domain\PayrollReport\ValueObject\BonusValueObject;

interface BonusCalculatorInterface
{
    public function calculate(Employee $employee): BonusValueObject;
}
