<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use App\Domain\PayrollReport\ValueObject\SalaryValueObject;

class PercentageBonusCalculator implements PercentageBonusCalculatorInterface
{
    public function calculate(
        SalaryValueObject $salary,
        BonusValueObject $bonus
    ): BonusValueObject
    {
        return new BonusValueObject((int) ($salary->value() * $bonus->value() / 100));
    }
}
