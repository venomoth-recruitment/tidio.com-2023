<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use DateTimeImmutable;

class FixedBonusCalculator implements FixedBonusCalculatorInterface
{
    private const MAX_BONUS_YEARS = 10;

    public function calculate(
        BonusValueObject $bonus,
        DateTimeImmutable $employmentDate
    ): BonusValueObject
    {
        $bonusYears = $this->calculateBonusYears($employmentDate);

        return new BonusValueObject($bonus->value() * $bonusYears);
    }

    private function calculateBonusYears($employmentDate): int
    {
        $currentDate = new DateTimeImmutable();
        $dateDiff = $currentDate->diff($employmentDate);
        $bonusYears = $dateDiff->y;

        if ($bonusYears > self::MAX_BONUS_YEARS) {
            $bonusYears = self::MAX_BONUS_YEARS;
        }

        return $bonusYears;
    }
}
