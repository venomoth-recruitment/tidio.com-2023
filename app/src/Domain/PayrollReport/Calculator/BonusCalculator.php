<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\Entity\Employee;
use App\Domain\PayrollReport\ValueObject\BonusTypeValueObject;
use App\Domain\PayrollReport\ValueObject\BonusValueObject;

class BonusCalculator implements BonusCalculatorInterface
{
    public function __construct(
        private readonly FixedBonusCalculatorInterface $fixedBonusCalculator,
        private readonly PercentageBonusCalculatorInterface $percentageBonusCalculator
    ) {
    }

    public function calculate(Employee $employee): BonusValueObject
    {
        $bonus = $employee->getDepartment()->getBonus();

        return match($bonus->getType()) {
            BonusTypeValueObject::fixed => 
                $this->fixedBonusCalculator->calculate($bonus->getValue(), $employee->getEmployedAt()),
            BonusTypeValueObject::percentage => 
                $this->percentageBonusCalculator->calculate($employee->getSalary(), $bonus->getValue())
        };
    }
}
