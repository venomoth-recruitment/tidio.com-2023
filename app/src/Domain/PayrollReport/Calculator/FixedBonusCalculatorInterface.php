<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use DateTimeImmutable;

interface FixedBonusCalculatorInterface
{
    public function calculate(
        BonusValueObject $bonus,
        DateTimeImmutable $employmentDate
    ): BonusValueObject;
}
