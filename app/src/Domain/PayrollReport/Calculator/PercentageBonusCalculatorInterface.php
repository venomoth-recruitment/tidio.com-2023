<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Calculator;

use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use App\Domain\PayrollReport\ValueObject\SalaryValueObject;

interface PercentageBonusCalculatorInterface
{
    public function calculate(
        SalaryValueObject $salary,
        BonusValueObject $bonus
    ): BonusValueObject;
}
