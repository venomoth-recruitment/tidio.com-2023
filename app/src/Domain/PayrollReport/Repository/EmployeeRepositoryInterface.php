<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Repository;

use App\Domain\PayrollReport\Collection\Employees;

interface EmployeeRepositoryInterface
{
    public function findAll(): Employees;
}
