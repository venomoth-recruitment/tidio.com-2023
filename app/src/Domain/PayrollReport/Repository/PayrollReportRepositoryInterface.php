<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Repository;

use App\Domain\PayrollReport\PayrollReportAggregate;
use App\Domain\Shared\Criteria\Criteria;
use App\Domain\Shared\ValueObject\IdValueObject;

interface PayrollReportRepositoryInterface
{
    public function store(PayrollReportAggregate $payrollReport): void;

    public function findBy(IdValueObject $id, Criteria $criteria): ?PayrollReportAggregate;
}
