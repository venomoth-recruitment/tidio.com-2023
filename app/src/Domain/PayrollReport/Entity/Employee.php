<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Entity;

use App\Domain\PayrollReport\ValueObject\EmployeeNameValueObject;
use App\Domain\PayrollReport\ValueObject\EmployeeSurnameValueObject;
use App\Domain\PayrollReport\ValueObject\SalaryValueObject;
use App\Domain\Shared\ValueObject\IdValueObject;
use DateTimeImmutable;

class Employee
{
    public function __construct(
        private readonly IdValueObject $id,
        private Department $department,
        private EmployeeNameValueObject $name,
        private EmployeeSurnameValueObject $surname,
        private SalaryValueObject $salary,
        private DateTimeImmutable $employedAt
    ) {
    }

    public static function fromValues(array $values): self
    {
        return new self(
            new IdValueObject($values['id']),
            Department::fromValues($values['department']),
            new EmployeeNameValueObject($values['name']),
            new EmployeeSurnameValueObject($values['surname']),
            new SalaryValueObject($values['salary']),
            DateTimeImmutable::createFromFormat('Y-m-d', $values['employed_at']),
        );
    }

    public function getId(): IdValueObject
    {
        return $this->id;
    }

    public function getDepartment(): Department
    {
        return $this->department;
    }

    public function getName(): EmployeeNameValueObject
    {
        return $this->name;
    }

    public function getSurname(): EmployeeSurnameValueObject
    {
        return $this->surname;
    }

    public function getSalary(): SalaryValueObject
    {
        return $this->salary;
    }

    public function getEmployedAt(): DateTimeImmutable
    {
        return $this->employedAt;
    }

    public function setDepartment(Department $department): void
    {
        $this->department = $department;
    }

    public function setName(EmployeeNameValueObject $name): void
    {
        $this->name = $name;
    }

    public function setSurname(EmployeeSurnameValueObject $surname): void
    {
        $this->surname = $surname;
    }

    public function setSalary(SalaryValueObject $salary): void
    {
        $this->salary = $salary;
    }

    public function setEmployedAt(DateTimeImmutable $employedAt): void
    {
        $this->employedAt = $employedAt;
    }
}
