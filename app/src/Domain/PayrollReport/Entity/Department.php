<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Entity;

use App\Domain\PayrollReport\ValueObject\DepartmentNameValueObject;
use App\Domain\Shared\ValueObject\IdValueObject;

class Department
{
    public function __construct(
        private readonly IdValueObject $id,
        private readonly Bonus $bonus,
        private DepartmentNameValueObject $name
    ) {
    }

    public static function fromValues(array $values): self
    {
        return new self(
            new IdValueObject($values['id']),
            Bonus::fromValues($values['bonus']),
            new DepartmentNameValueObject($values['name'])
        );
    }

    public function getId(): IdValueObject
    {
        return $this->id;
    }

    public function getBonus(): Bonus
    {
        return $this->bonus;
    }

    public function getName(): DepartmentNameValueObject
    {
        return $this->name;
    }

    public function setBonus(Bonus $bonus): void
    {
        $this->bonus = $bonus;
    }

    public function setName(DepartmentNameValueObject $name): void
    {
        $this->name = $name;
    }
}
