<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Entity;

use App\Domain\PayrollReport\ValueObject\BonusNameValueObject;
use App\Domain\PayrollReport\ValueObject\BonusTypeValueObject;
use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use App\Domain\Shared\ValueObject\IdValueObject;

class Bonus
{
    public function __construct(
        private readonly IdValueObject $id,
        private BonusNameValueObject $name,
        private BonusTypeValueObject $type,
        private BonusValueObject $value
    ) {
    }

    public static function fromValues(array $values): self
    {
        return new self(
            new IdValueObject($values['id']),
            new BonusNameValueObject($values['name']),
            constant("App\Domain\PayrollReport\ValueObject\BonusTypeValueObject::{$values['type']}"),
            new BonusValueObject($values['value'])
        );
    }

    public function getId(): IdValueObject
    {
        return $this->id;
    }

    public function getName(): BonusNameValueObject
    {
        return $this->name;
    }

    public function getType(): BonusTypeValueObject
    {
        return $this->type;
    }

    public function getValue(): BonusValueObject
    {
        return $this->value;
    }

    public function setName(BonusNameValueObject $name): void
    {
        $this->name = $name;
    }

    public function setType(BonusTypeValueObject $type): void
    {
        $this->type = $type;
    }

    public function setValue(BonusValueObject $value): void
    {
        $this->value = $value;
    }
}
