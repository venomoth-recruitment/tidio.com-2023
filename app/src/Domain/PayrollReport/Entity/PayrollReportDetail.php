<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Entity;

use App\Domain\PayrollReport\ValueObject\BonusTypeValueObject;
use App\Domain\PayrollReport\ValueObject\BonusValueObject;
use App\Domain\PayrollReport\ValueObject\DepartmentNameValueObject;
use App\Domain\PayrollReport\ValueObject\EmployeeNameValueObject;
use App\Domain\PayrollReport\ValueObject\EmployeeSurnameValueObject;
use App\Domain\PayrollReport\ValueObject\SalaryValueObject;
use App\Domain\Shared\ValueObject\IdValueObject;

class PayrollReportDetail
{
    public function __construct(
        private readonly IdValueObject $id,
        private EmployeeNameValueObject $employeeName,
        private EmployeeSurnameValueObject $employeeSurname,
        private DepartmentNameValueObject $departmentName,
        private SalaryValueObject $salary,
        private BonusTypeValueObject $bonusType,
        private BonusValueObject $bonus,
        private SalaryValueObject $totalSalary
    ) {
    }

    public static function fromValues(array $values): self
    {
        return new self(
            new IdValueObject($values['id']),
            new EmployeeNameValueObject($values['employee_name']),
            new EmployeeSurnameValueObject($values['employee_surname']),
            new DepartmentNameValueObject($values['department']),
            new SalaryValueObject($values['base_salary']),
            constant("App\Domain\PayrollReport\ValueObject\BonusTypeValueObject::{$values['bonus_type']}"),
            new BonusValueObject($values['bonus']),
            new SalaryValueObject($values['total_salary'])
        );
    }

    public function getId(): IdValueObject
    {
        return $this->id;
    }

    public function getEmployeeName(): EmployeeNameValueObject
    {
        return $this->employeeName;
    }

    public function getEmployeeSurname(): EmployeeSurnameValueObject
    {
        return $this->employeeSurname;
    }

    public function getDepartmentName(): DepartmentNameValueObject
    {
        return $this->departmentName;
    }

    public function getSalary(): SalaryValueObject
    {
        return $this->salary;
    }

    public function getBonusType(): BonusTypeValueObject
    {
        return $this->bonusType;
    }

    public function getBonus(): BonusValueObject
    {
        return $this->bonus;
    }

    public function getTotalSalary(): SalaryValueObject
    {
        return $this->totalSalary;
    }

    public function setEmployeeName(EmployeeNameValueObject $employeeName): void
    {
        $this->employeeName = $employeeName;
    }

    public function setEmployeeSurname(EmployeeSurnameValueObject $employeeSurname): void
    {
        $this->employeeSurname = $employeeSurname;
    }

    public function setDepartmentName(DepartmentNameValueObject $departmentName): void
    {
        $this->departmentName = $departmentName;
    }

    public function setSalary(SalaryValueObject $salary): void
    {
        $this->salary = $salary;
    }

    public function setBonusType(BonusTypeValueObject $bonusType): void
    {
        $this->bonusType = $bonusType;
    }

    public function setBonus(BonusValueObject $bonus): void
    {
        $this->bonus = $bonus;
    }

    public function setTotalSalary(SalaryValueObject $totalSalary): void
    {
        $this->totalSalary = $totalSalary;
    }
}
