<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport;

use App\Domain\PayrollReport\Collection\PayrollReportDetails;
use App\Domain\Shared\ValueObject\IdValueObject;
use DateTimeImmutable;

class PayrollReportAggregate
{
    public function __construct(
        private readonly IdValueObject $id,
        private DateTimeImmutable $date,
        private PayrollReportDetails $details
    ) {
    }

    public function getId(): IdValueObject
    {
        return $this->id;
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function getDetails(): PayrollReportDetails
    {
        return $this->details;
    }

    public function setDate(DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

    public function setDetails(PayrollReportDetails $details): void
    {
        $this->details = $details;
    }
}
