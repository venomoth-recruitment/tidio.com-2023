<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Service;

interface PayrollReportServiceInterface
{
    public function create(): void;
}
