<?php

declare(strict_types=1);

namespace App\Domain\PayrollReport\Service;

use App\Domain\PayrollReport\Calculator\BonusCalculatorInterface;
use App\Domain\PayrollReport\Collection\Employees;
use App\Domain\PayrollReport\Collection\PayrollReportDetails;
use App\Domain\PayrollReport\Entity\PayrollReportDetail;
use App\Domain\PayrollReport\PayrollReportAggregate;
use App\Domain\PayrollReport\Repository\EmployeeRepositoryInterface;
use App\Domain\PayrollReport\Repository\PayrollReportRepositoryInterface;
use App\Domain\PayrollReport\ValueObject\SalaryValueObject;
use App\Domain\Shared\Utils\UuidGeneratorInterface;
use App\Domain\Shared\ValueObject\IdValueObject;
use DateTimeImmutable;

class PayrollReportService implements PayrollReportServiceInterface
{
    public function __construct(
        private readonly UuidGeneratorInterface $uuidGenerator,
        private readonly EmployeeRepositoryInterface $employeeRepository,
        private readonly PayrollReportRepositoryInterface $payrollReportRepository,
        private readonly BonusCalculatorInterface $bonusCalculator
    ) {
    }

    public function create(): void
    {
        $employees = $this->employeeRepository->findAll();

        $payrollReport = new PayrollReportAggregate(
            new IdValueObject($this->uuidGenerator->generate()),
            new DateTimeImmutable(),
            $this->createDetails($employees)
        );

        $this->payrollReportRepository->store($payrollReport);
    }

    private function createDetails(Employees $employees): PayrollReportDetails
    {
        $details = [];

        foreach ($employees as $employee) {
            $bonus = $this->bonusCalculator->calculate($employee);

            $details[] = new PayrollReportDetail(
                new IdValueObject($this->uuidGenerator->generate()),
                $employee->getName(),
                $employee->getSurname(),
                $employee->getDepartment()->getName(),
                $employee->getSalary(),
                $employee->getDepartment()->getBonus()->getType(),
                $bonus,
                new SalaryValueObject($employee->getSalary()->value() + $bonus->value())
            );
        }

        return new PayrollReportDetails($details);
    }
}
