<?php

declare(strict_types=1);

namespace App\Application\Query;

readonly class GetPayrollReportQuery
{
    public function __construct(
        public string $id,
        public ?string $name,
        public ?string $surname,
        public ?string $department,
        public ?string $sortBy,
        public ?string $orderBy
    ) {
    }
}
