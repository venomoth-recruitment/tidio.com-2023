<?php

declare(strict_types=1);

namespace App\Application\Query\Handler;

use App\Application\Query\GetPayrollReportQuery;
use App\Application\Query\View\GetPayrollReportView;

interface GetPayrollReportQueryHandlerInterface
{
    public function handle(GetPayrollReportQuery $query): GetPayrollReportView;
}
