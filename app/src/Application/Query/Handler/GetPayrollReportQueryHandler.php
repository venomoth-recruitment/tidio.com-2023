<?php

declare(strict_types=1);

namespace App\Application\Query\Handler;

use App\Application\Query\GetPayrollReportQuery;
use App\Application\Query\View\GetPayrollReportView;
use App\Domain\PayrollReport\Repository\PayrollReportRepositoryInterface;
use App\Domain\Shared\Criteria\Criteria;
use App\Domain\Shared\Criteria\FilterOperator;
use App\Domain\Shared\Criteria\Filters;
use App\Domain\Shared\Criteria\Order;
use App\Domain\Shared\Criteria\OrderBy;
use App\Domain\Shared\Criteria\SortBy;
use App\Domain\Shared\ValueObject\IdValueObject;

class GetPayrollReportQueryHandler implements GetPayrollReportQueryHandlerInterface
{
    public function __construct(private readonly PayrollReportRepositoryInterface $repository)
    {
    }

    public function handle(GetPayrollReportQuery $query): GetPayrollReportView
    {
        $payrollReportAggregate = $this->repository->findBy(
            new IdValueObject($query->id),
            new Criteria($this->buildFilters($query), $this->buildOrder($query))
        );

        return GetPayrollReportView::fromPayrollReportAggregate($payrollReportAggregate);
    }

    private function buildFilters(GetPayrollReportQuery $query): Filters
    {
        $filters = [];

        if ($query->name !== null) {
            $filters[] = [
                'field' => 'employee_name',
                'operator' => FilterOperator::EQUAL,
                'value' => $query->name
            ];
        }

        if ($query->surname !== null) {
            $filters[] = [
                'field' => 'employee_surname',
                'operator' => FilterOperator::EQUAL,
                'value' => $query->surname
            ];
        }

        if ($query->department !== null) {
            $filters[] = [
                'field' => 'department',
                'operator' => FilterOperator::EQUAL,
                'value' => $query->department
            ];
        }

        return Filters::fromValues($filters);
    }

    private function buildOrder(GetPayrollReportQuery $query): ?Order
    {
        if ($query->sortBy === null) {
            return null;
        }

        $sortBy = match($query->sortBy) {
            'name' => 'employee_name',
            'surname' => 'employee_surname',
            'department' => 'department',
            'base_salary' => 'base_salary',
            'bonus' => 'bonus',
            'bonus_type' => 'bonus_type',
            'total_salary' => 'total_salary'
        };

        if ($query->orderBy === null) {
            $orderBy = OrderBy::asc;
        } else {
            $orderBy = $query->orderBy === 'desc' ? OrderBy::desc : OrderBy::asc;
        }

        return new Order(new SortBy($sortBy), $orderBy);
    }
}
