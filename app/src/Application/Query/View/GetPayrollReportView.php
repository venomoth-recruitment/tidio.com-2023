<?php

declare(strict_types=1);

namespace App\Application\Query\View;

use App\Domain\PayrollReport\PayrollReportAggregate;

readonly class GetPayrollReportView
{
    private function __construct(public array $data)
    {
    }

    public static function fromPayrollReportAggregate(?PayrollReportAggregate $payrollReportAggregate): self
    {
        $data = [];

        if ($payrollReportAggregate !== null) {
            foreach ($payrollReportAggregate->getDetails()->payrollReportDetails() as $detail) {
                $data[] = [
                    'name' => $detail->getEmployeeName()->value(),
                    'surname' => $detail->getEmployeeSurname()->value(),
                    'department' => $detail->getDepartmentName()->value(),
                    'base_salary' => $detail->getSalary()->value(),
                    'bonus_type' => $detail->getBonusType()->name,
                    'bonus' => $detail->getBonus()->value(),
                    'total_salary' => $detail->getTotalSalary()->value()
                ];
            }
        }

        return new self($data);
    }
}
