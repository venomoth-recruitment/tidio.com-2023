<?php

declare(strict_types=1);

namespace App\Application\Command\Handler;

use App\Application\Command\CreatePayrollReportCommand;
use App\Domain\PayrollReport\Service\PayrollReportServiceInterface;

class CreatePayrollReportCommandHandler implements CreatePayrollReportCommandHandlerInterface
{
    public function __construct(private readonly PayrollReportServiceInterface $payrollReportService)
    {
    }

    public function handle(CreatePayrollReportCommand $command): void
    {
        $this->payrollReportService->create();
    }
}
