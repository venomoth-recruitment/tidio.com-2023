<?php

declare(strict_types=1);

namespace App\Application\Command\Handler;

use App\Application\Command\CreatePayrollReportCommand;

interface CreatePayrollReportCommandHandlerInterface
{
    public function handle(CreatePayrollReportCommand $command): void;
}
