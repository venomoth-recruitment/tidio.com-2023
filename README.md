# tidio.com-2023

## Installation

### Run Docker containers

```shell
docker-compose up -d
```

### Install Composer packages
```shell
docker-compose exec php composer install
```

## Run application

### Get report

Go to [http://127.0.0.1:8080/payroll_reports/b0ecb638-7e99-4d68-a0ea-5f94b7cd3043](http://127.0.0.1:8080/payroll_reports/b0ecb638-7e99-4d68-a0ea-5f94b7cd3043) in your browser or call the endpoint with optional parameters using other tools:

```
GET /payroll_reports/{id}?name=ania&sort_by=name&order_by=asc
```

### Create report

Report for current date can be created by running console command:

```
docker-compose exec php bin/console app:create-payroll-report
```

## Run tests
```shell
docker-compose exec php vendor/bin/phpunit
```
